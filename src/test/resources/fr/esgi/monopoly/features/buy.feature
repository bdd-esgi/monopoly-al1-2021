@default_properties
Feature: buy a property

  Scenario Outline: Buying property
    Given I land on unowned <name>
    And my funds are <funds>
    When I buy the property
    Then I should receive the <name> title deed
    And my funds should be <remaining>
    Examples:
      | name   | funds | remaining |
      | "west" | 2000  | 1800      |
      | "east" | 2000  | 1950      |
