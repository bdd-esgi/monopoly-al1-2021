Feature: Definitions

  @default_properties
  Scenario: Default properties
    Then the available properties should be at least:
      | name | price |
      | west | 200   |
      | east | 50    |
