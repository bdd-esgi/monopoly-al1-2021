package fr.esgi.monopoly.model;


import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class PlayerTest {

    @Test
    void should_decrease_fund() {
        final Player player = new Player(10000);
        player.decreaseFunds(1000);
        Assertions.assertThat(player.funds()).isEqualTo(9000);
    }

    @Test
    void should_not_be_bankrout() {
        final Player player = new Player(1000);
        player.decreaseFunds(2000);
        Assertions.assertThat(player.funds()).isEqualTo(1000);
    }
}
