package fr.esgi.monopoly.features;

import fr.esgi.monopoly.model.Property;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.assertj.core.api.Assertions;

public class BuySteps {

    private final FeatureContext featureContext;
    private Property landingProperty;

    public BuySteps(final FeatureContext featureContext) {
        this.featureContext = featureContext;
    }

    @Given("I land on unowned {string}")
    public void iLandOnUnownedPropertyThatCost(String propertyName) {
        this.landingProperty = getProperty(propertyName);
    }

    private Property getProperty(final String propertyName) {
        return featureContext.getProperty(propertyName).orElseThrow(() -> new IllegalArgumentException(propertyName + " does not exist in board"));
    }

    @And("my funds are {int}")
    public void myFundsAre(int funds) {
        featureContext.currentPlayer().setFunds(funds);
    }

    @When("I buy the property")
    public void iBuyTheProperty() {
        featureContext.game().buyProperty(featureContext.currentPlayer(), landingProperty);
    }

    @Then("I should receive the {string} title deed")
    public void iShouldReceiveTheTitleDeed(String propertyName) {
        Assertions.assertThat(featureContext.currentPlayer().titles()).contains(getProperty(propertyName));
    }

    @And("my funds should be {int}")
    public void myFundsShouldBe(int funds) {
        Assertions.assertThat(featureContext.currentPlayer().funds()).isEqualTo(funds);
    }
}
