package fr.esgi.monopoly.features;

import fr.esgi.monopoly.model.Property;
import io.cucumber.java.Before;
import io.cucumber.java.DataTableType;
import io.cucumber.java.en.Then;
import org.assertj.core.api.Assertions;

import java.util.List;
import java.util.Map;

public class DefaultPropertiesSteps {

    private final FeatureContext featureContext;

    public DefaultPropertiesSteps(final FeatureContext featureContext) {
        this.featureContext = featureContext;
    }

    @Before("@default_properties")
    public void defaultPropertiesAreUsed() {
        final Property west = new Property("west", 200);
        final Property east = new Property("east", 50);
        featureContext.addPropertyToBoard(west);
        featureContext.addPropertyToBoard(east);
    }

    @Then("the available properties should be at least:")
    public void theAvailablePropertiesShouldBeAtLeast(List<Property> properties) {
        Assertions.assertThat(featureContext.properties()).containsAll(properties);
    }

    @DataTableType
    public Property convert(Map<String, String> table) {
        return new Property(table.get("name"), Integer.parseInt(table.get("price")));
    }
}
