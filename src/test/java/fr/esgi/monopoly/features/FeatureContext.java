package fr.esgi.monopoly.features;

import fr.esgi.monopoly.model.Board;
import fr.esgi.monopoly.model.Game;
import fr.esgi.monopoly.model.Player;
import fr.esgi.monopoly.model.Property;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class FeatureContext {

    private Game game = new Game(new Board(List.of()), List.of());
    private Player currentPlayer = new Player();

    public void addPropertyToBoard(Property property) {
        List<Property> properties = new ArrayList<>(game.board().properties());
        properties.add(property);

        game = new Game(new Board(properties), game.players());
    }

    public List<Property> properties() {
        return game.board().properties();
    }

    public Optional<Property> getProperty(final String propertyName) {
        return game.board().getProperty(propertyName);
    }

    public Game game() {
        return game;
    }

    public Player currentPlayer() {
        return currentPlayer;
    }
}
