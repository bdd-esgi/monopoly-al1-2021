package fr.esgi.monopoly;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"src/test/resources/fr/esgi/monopoly/features"}
        ,glue = {"fr.esgi.monopoly.features"}
        ,tags = "not @wip"
)
public class RunFeatureTest {
}
