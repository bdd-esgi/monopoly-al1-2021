package fr.esgi.monopoly.model;

import java.util.ArrayList;
import java.util.List;

public class Player {

    private int funds;
    private List<Property> titles = new ArrayList<>();

    public Player() {
        this.funds = 40000;
    }

    public Player(final int funds) {
        this.funds = funds;
    }

    public int funds() {
        return funds;
    }

    public void setFunds(final int funds) {
        this.funds = funds;
    }

    public List<Property> titles() {
        return titles;
    }

    public void decreaseFunds(final int price) {
        this.funds = (funds - price) > 0 ? (funds - price) : funds;
    }

    public void addTitle(final Property property) {
        titles.add(property);
    }

    public void buyProperty(Property property) {
        //TODO gérer le cas fonds insuffisant
        decreaseFunds(property.price());
        addTitle(property);
    }
}
