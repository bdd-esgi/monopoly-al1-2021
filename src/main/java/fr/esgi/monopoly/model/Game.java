package fr.esgi.monopoly.model;

import java.util.List;

public class Game {

    private final Board board;
    //    private final Bank bank;
    private final List<Player> players;

    public Game(final Board board, final List<Player> players) {
        this.board = board;
        this.players = players;
    }

    public Board board() {
        return board;
    }

    public List<Player> players() {
        return players;
    }

    public void buyProperty(Player player, Property property) {
        player.buyProperty(property);
        // bank.putMoney(property.price())
    }
}
