package fr.esgi.monopoly.model;

import java.util.Objects;

public class Property {

    private final String name;
    private final int price;

    public Property(final String name, final int price) {
        this.name = name;
        this.price = price;
    }

    public String name() {
        return name;
    }

    public int price() {
        return price;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (!(o instanceof Property)) return false;
        final Property property = (Property) o;
        return price == property.price && Objects.equals(name, property.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, price);
    }

    @Override
    public String toString() {
        return "Property{" +
                "name='" + name + '\'' +
                ", price=" + price +
                '}';
    }
}
