package fr.esgi.monopoly.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Board {

    private final List<Property> properties;

    public Board(final List<Property> properties) {
        this.properties = properties;
    }

    public List<Property> properties() {
        return new ArrayList<>(properties);
    }

    public Optional<Property> getProperty(final String propertyName) {
        return properties.stream().filter(p->p.name().equals(propertyName)).findAny();
    }

}
